#!/usr/bin/env bash
# Script used to sync entire gitlab group/personal repos to gitea
# cp .env.example .env
# set required vars
source .env

if [ -z "$GITLAB_PRIVATE_TOKEN" ]; then
    echo "Please set the environment variable GITLAB_PRIVATE_TOKEN"
    echo "See ${BASE_PATH}profile/account"
    exit 1
fi

FILENAME="repos.json"

trap "{ rm -f $FILENAME; }" EXIT

curl -s "${BASE_PATH}api/v4/projects?private_token=$GITLAB_PRIVATE_TOKEN&search=$PROJECT_SEARCH_PARAM&per_page=999&owned=true" \
    | jq --raw-output --compact-output ".[] | $PROJECT_SELECTION | $PROJECT_PROJECTION" > "$FILENAME"

while read repo; do
    URL=$(echo "$repo" | jq -r ".git")
    REPO_NAME=$(echo "$repo" | jq -r ".path")

    echo "Found $URL, importing..."

    curl -X POST "https://$GITEA_DOMAIN/api/v1/repos/migrate" -u $GITEA_USERNAME:$GITEA_TOKEN -H  "accept: application/json" -H  "Content-Type: application/json" -H "Authorization: token $GITEA_TOKEN" -d "{  \
    \"auth_username\": \"$GITLAB_USERNAME\", \
    \"auth_password\": \"$GITLAB_PRIVATE_TOKEN\", \
    \"clone_addr\": \"$URL\", \
    \"mirror\": true, \
    \"private\": true, \
    \"repo_name\": \"$REPO_NAME\", \
    \"repo_owner\": \"$GITEA_REPO_OWNER\", \
    \"service\": \"git\", \
    \"uid\": 0, \
    \"wiki\": false}"
done < "$FILENAME"
